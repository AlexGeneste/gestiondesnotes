<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Etudiant;

class EtudiantFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i=1;$i<=10;$i++) {
            $etudiant = new Etudiant();

            $etudiant->setName("Nom$i")->setFirstname("prenom$i")->setEmail("email@$i.com");
            $manager->persist($etudiant);
        }

        $manager->flush();
    }
}
